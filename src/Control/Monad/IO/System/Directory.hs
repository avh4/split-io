module Control.Monad.IO.System.Directory
  ( MonadDirectoryRO (..),
    MonadDirectoryRW (..),
    MonadWorkingDirectoryRO (..),
    MonadWorkingDirectoryRW (..),
    MonadPredefinedDirectories (..),
    MonadFindExecutables (..),
    MonadFindFiles (..),
    MonadSymlinkRW (..),
    MonadSymlinkRO (..),
    MonadDirectoryMetadataRO (..),
    MonadDirectoryMetadataRW (..),
    module System.Directory,
  )
where

import Data.Maybe (listToMaybe, maybeToList)
import Data.Time (UTCTime)
import System.Directory (Permissions (..), XdgDirectory (..), XdgDirectoryList (..), emptyPermissions, exeExtension, executable, readable, searchable, setOwnerExecutable, setOwnerReadable, setOwnerSearchable, setOwnerWritable, writable)
import qualified System.Directory

class Monad m => MonadDirectoryRO m where
  listDirectory :: FilePath -> m [FilePath]
  getDirectoryContents :: FilePath -> m [FilePath]
  getFileSize :: FilePath -> m Integer
  doesPathExist :: FilePath -> m Bool
  doesFileExist :: FilePath -> m Bool
  doesDirectoryExist :: FilePath -> m Bool

instance MonadDirectoryRO IO where
  listDirectory = System.Directory.listDirectory
  getDirectoryContents = System.Directory.getDirectoryContents
  getFileSize = System.Directory.getFileSize
  doesPathExist = System.Directory.doesPathExist
  doesFileExist = System.Directory.doesFileExist
  doesDirectoryExist = System.Directory.doesDirectoryExist

class MonadDirectoryRO m => MonadDirectoryRW m where
  createDirectory :: FilePath -> m ()
  createDirectoryIfMissing :: Bool -> FilePath -> m ()
  removeDirectory :: FilePath -> m ()
  removeDirectoryRecursive :: FilePath -> m ()
  removePathForcibly :: FilePath -> m ()
  renameDirectory :: FilePath -> FilePath -> m ()

  removeFile :: FilePath -> m ()
  renameFile :: FilePath -> FilePath -> m ()
  renamePath :: FilePath -> FilePath -> m ()
  copyFile :: FilePath -> FilePath -> m ()

instance MonadDirectoryRW IO where
  createDirectory = System.Directory.createDirectory
  createDirectoryIfMissing = System.Directory.createDirectoryIfMissing
  removeDirectory = System.Directory.removeDirectory
  removeDirectoryRecursive = System.Directory.removeDirectoryRecursive
  removePathForcibly = System.Directory.removePathForcibly
  renameDirectory = System.Directory.renameDirectory

  removeFile = System.Directory.removeFile
  renameFile = System.Directory.renameFile
  renamePath = System.Directory.renamePath
  copyFile = System.Directory.copyFile

class Monad m => MonadWorkingDirectoryRO m where
  getCurrentDirectory :: m FilePath
  canonicalizePath :: FilePath -> m FilePath
  makeAbsolute :: FilePath -> m FilePath
  makeRelativeToCurrentDirectory :: FilePath -> m FilePath

instance MonadWorkingDirectoryRO IO where
  getCurrentDirectory = System.Directory.getCurrentDirectory
  canonicalizePath = System.Directory.canonicalizePath
  makeAbsolute = System.Directory.makeAbsolute
  makeRelativeToCurrentDirectory = System.Directory.makeRelativeToCurrentDirectory

class MonadWorkingDirectoryRO m => MonadWorkingDirectoryRW m where
  setCurrentDirectory :: FilePath -> m ()
  withCurrentDirectory :: FilePath -> m a -> m a

instance MonadWorkingDirectoryRW IO where
  setCurrentDirectory = System.Directory.setCurrentDirectory
  withCurrentDirectory = System.Directory.withCurrentDirectory

class Monad m => MonadPredefinedDirectories m where
  getHomeDirectory :: m FilePath
  getXdgDirectory :: XdgDirectory -> FilePath -> m FilePath
  getXdgDirectoryList :: XdgDirectoryList -> m [FilePath]
  getAppUserDataDirectory :: FilePath -> m FilePath
  getUserDocumentsDirectory :: m FilePath
  getTemporaryDirectory :: m FilePath

instance MonadPredefinedDirectories IO where
  getHomeDirectory = System.Directory.getHomeDirectory
  getXdgDirectory = System.Directory.getXdgDirectory
  getXdgDirectoryList = System.Directory.getXdgDirectoryList
  getAppUserDataDirectory = System.Directory.getAppUserDataDirectory
  getUserDocumentsDirectory = System.Directory.getUserDocumentsDirectory
  getTemporaryDirectory = System.Directory.getTemporaryDirectory

class MonadDirectoryRO m => MonadFindExecutables m where
  findExecutable :: String -> m (Maybe FilePath)
  findExecutable = fmap listToMaybe . findExecutables
  findExecutables :: String -> m [FilePath]
  findExecutables = fmap maybeToList . findExecutable

instance MonadFindExecutables IO where
  findExecutable = System.Directory.findExecutable
  findExecutables = System.Directory.findExecutables

class MonadDirectoryRO m => MonadFindFiles m where
  findExecutablesInDirectories :: [FilePath] -> String -> m [FilePath]
  findFile :: [FilePath] -> String -> m (Maybe FilePath)
  findFileWith :: (FilePath -> m Bool) -> [FilePath] -> String -> m (Maybe FilePath)
  findFileWith b p = fmap listToMaybe . findFilesWith b p
  findFilesWith :: (FilePath -> m Bool) -> [FilePath] -> String -> m [FilePath]
  findFilesWith b p = fmap maybeToList . findFileWith b p

instance MonadFindFiles IO where
  findExecutablesInDirectories = System.Directory.findExecutablesInDirectories
  findFile = System.Directory.findFile
  findFileWith = System.Directory.findFileWith
  findFilesWith = System.Directory.findFilesWith

class MonadDirectoryRO m => MonadSymlinkRO m where
  pathIsSymbolicLink :: FilePath -> m Bool
  getSymbolicLinkTarget :: FilePath -> m FilePath

class MonadDirectoryRW m => MonadSymlinkRW m where
  createFileLink :: FilePath -> FilePath -> m ()
  createDirectoryLink :: FilePath -> FilePath -> m ()
  removeDirectoryLink :: FilePath -> m ()

instance MonadSymlinkRW IO where
  createFileLink = System.Directory.createFileLink
  createDirectoryLink = System.Directory.createDirectoryLink
  removeDirectoryLink = System.Directory.removeDirectoryLink

class MonadDirectoryRO m => MonadDirectoryMetadataRO m where
  getPermissions :: FilePath -> m Permissions
  getAccessTime :: FilePath -> m UTCTime
  getModificationTime :: FilePath -> m UTCTime

instance MonadDirectoryMetadataRO IO where
  getPermissions = System.Directory.getPermissions
  getAccessTime = System.Directory.getAccessTime
  getModificationTime = System.Directory.getModificationTime

class (MonadDirectoryRW m, MonadDirectoryMetadataRO m) => MonadDirectoryMetadataRW m where
  copyFileWithMetadata :: FilePath -> FilePath -> m ()

  setPermissions :: FilePath -> Permissions -> m ()
  copyPermissions :: FilePath -> FilePath -> m ()
  copyPermissions a b = getPermissions a >>= setPermissions b
  setAccessTime :: FilePath -> UTCTime -> m ()
  setModificationTime :: FilePath -> UTCTime -> m ()

instance MonadDirectoryMetadataRW IO where
  copyFileWithMetadata = System.Directory.copyFileWithMetadata

  setPermissions = System.Directory.setPermissions
  copyPermissions = System.Directory.copyPermissions
  setAccessTime = System.Directory.setAccessTime
  setModificationTime = System.Directory.setModificationTime
