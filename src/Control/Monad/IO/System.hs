{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}

{-# HLINT ignore "Use print" #-}
{-# HLINT ignore "Use hPrint" #-}
{-# HLINT ignore "Use putStr" #-}

module Control.Monad.IO.System
  ( MonadFileRO (..),
    MonadFileROLazy (..),
    MonadFileRW (..),
    MonadFileHandle (..),
    MonadFileAppend (..),
    MonadFileHandleRead (..),
    MonadStdinEOF (..),
    MonadStdin (..),
    MonadStdinLazy (..),
    MonadStdinString (..),
    MonadStdout (..),
    MonadStdoutString (..),
    MonadStdinout (..),
    MonadFileHandleSeek (..),
    MonadFileHandleFlush (..),
    module System.IO,
  )
where

import Data.ByteString (ByteString)
import qualified Data.ByteString
import Data.Text (Text)
import qualified Data.Text.IO
import System.IO (BufferMode, Handle, HandlePosn, IOMode (..), SeekMode)
import qualified System.IO
import Prelude (Bool, Char, FilePath, IO, Int, Integer, Monad, Read, Show, String, show, (.), (>>=))

class Monad m => MonadFileRO a m where
  readFile :: FilePath -> m a

instance MonadFileRO String IO where
  readFile = System.IO.readFile'

instance MonadFileRO ByteString IO where
  readFile = Data.ByteString.readFile

instance MonadFileRO Text IO where
  readFile = Data.Text.IO.readFile

class Monad m => MonadFileROLazy a m where
  readFileLazy :: FilePath -> m a

instance MonadFileROLazy String IO where
  readFileLazy = System.IO.readFile

class MonadFileRO a m => MonadFileRW a m where
  writeFile :: FilePath -> a -> m ()

instance MonadFileRW String IO where
  writeFile = System.IO.writeFile

instance MonadFileRW ByteString IO where
  writeFile = Data.ByteString.writeFile

instance MonadFileRW Text IO where
  writeFile = Data.Text.IO.writeFile

class MonadFileRO a m => MonadFileAppend a m where
  appendFile :: FilePath -> a -> m ()

instance MonadFileAppend String IO where
  appendFile = System.IO.appendFile

instance MonadFileAppend ByteString IO where
  appendFile = Data.ByteString.appendFile

instance MonadFileAppend Text IO where
  appendFile = Data.Text.IO.appendFile

class Monad m => MonadStdinEOF m where
  isEOF :: m Bool

instance MonadStdinEOF IO where
  isEOF = System.IO.isEOF

class MonadStdinEOF m => MonadStdin a m where
  getLine :: m a
  getContents :: m a

class MonadStdinEOF m => MonadStdinLazy a m where
  getContentsLazy :: m a

instance MonadStdinLazy String IO where
  getContentsLazy = System.IO.getContents

instance MonadStdin String IO where
  getLine = System.IO.getLine
  getContents = System.IO.getContents'

-- getContents' = System.IO.getContents'

instance MonadStdin ByteString IO where
  getLine = Data.ByteString.getLine
  getContents = Data.ByteString.getContents

class MonadStdin String m => MonadStdinString m where
  getChar :: m Char
  readIO :: Read a => String -> m a
  readLn :: Read a => m a
  readLn = getLine >>= readIO

instance MonadStdinString IO where
  getChar = System.IO.getChar
  readIO = System.IO.readIO
  readLn = System.IO.readLn

class Monad m => MonadStdout a m where
  putStr :: a -> m ()

instance MonadStdout String IO where
  putStr = System.IO.putStr

instance MonadStdout ByteString IO where
  putStr = Data.ByteString.putStr

class MonadStdout String m => MonadStdoutString m where
  putChar :: Char -> m ()
  putChar c = putStr [c]
  putStrLn :: String -> m ()
  print :: Show a => a -> m ()
  print = putStrLn . show

instance MonadStdoutString IO where
  putChar = System.IO.putChar
  putStrLn = System.IO.putStrLn
  print = System.IO.print

class (MonadStdin a m, MonadStdout a m) => MonadStdinout a m where
  interact :: (a -> a) -> m ()

instance MonadStdinout ByteString IO where
  interact = Data.ByteString.interact

class MonadStdin String m => MonadFileHandleRead m where
  hFileSize :: Handle -> m Integer
  hIsEOF :: Handle -> m Bool
  hIsOpen :: Handle -> m Bool
  hIsClosed :: Handle -> m Bool
  hIsReadable :: Handle -> m Bool

  hWaitForInput :: Handle -> Int -> m Bool
  hReady :: Handle -> m Bool
  hGetChar :: Handle -> m Char
  hGetLine :: Handle -> m String
  hLookAhead :: Handle -> m Char
  hGetContents :: Handle -> m String

-- hGetContents' :: Handle -> IO String

class MonadFileHandleRead m => MonadFileHandleFlush m where
  hClose :: Handle -> m ()
  hGetBuffering :: Handle -> m BufferMode
  hSetBuffering :: Handle -> BufferMode -> m ()
  hFlush :: Handle -> m ()

class MonadFileHandleRead m => MonadFileHandleSeek m where
  hGetPosn :: Handle -> m HandlePosn
  hSetPosn :: HandlePosn -> m ()
  hSeek :: Handle -> SeekMode -> Integer -> m ()
  hTell :: Handle -> m Integer
  hIsSeekable :: Handle -> m Bool

class MonadFileHandleRead m => MonadFileHandle m where
  withFile :: FilePath -> IOMode -> (Handle -> m r) -> m r
  openFile :: FilePath -> IOMode -> m Handle

  hSetFileSize :: Handle -> Integer -> m ()
  hIsWritable :: Handle -> m Bool

  hPutChar :: Handle -> Char -> m ()
  hPutStr :: Handle -> String -> m ()
  hPutStrLn :: Handle -> String -> m ()
  hPrint :: Show a => Handle -> a -> m ()
  hPrint h = hPutStrLn h . show

-- Binary files https://hackage.haskell.org/package/base-4.18.0.0/docs/System-IO.html#g:21
-- Temporary files https://hackage.haskell.org/package/base-4.18.0.0/docs/System-IO.html#g:22
-- ...
