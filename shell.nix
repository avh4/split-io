args @ {compiler ? "ghc925", ...}: let
  default = (import ./. args)."${compiler}";
  inherit (default) pkgs haskellPackages haskellTools;
in
  haskellPackages.shellFor {
    name = "split-io";
    packages = p: [p.split-io];
    buildInputs = with pkgs; [
      # Tools required to build
      cabal-install

      # Dev tools
      alejandra
      git
      hpack
      niv
      simple-http-server
      # Must use the same ghc as the project
      haskellPackages.ghcid
      haskellPackages.haskell-language-server
    ];
  }
