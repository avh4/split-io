This package provides typeclasses that split `IO` into smaller,
more focused classes.


## Future improvements

I've only added as much as needed for me to use this for testing other applications I develop.
Contributions to add any of the following are welcome:

  - Add default implementations in terms of other functions within the same class, where possible
  - Add support for more functions
  - Better organize the functions into classes
  - Support older versions of dependencies, if needed


# Comparison to ...

  - [IOSpec: A pure specification of the IO monad.](https://hackage.haskell.org/package/IOSpec): provides only a new testing monad, and does not provide focused classes
