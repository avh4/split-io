{sources ? import ./nix/sources.nix}: let
  gitignore = import sources."gitignore.nix" {};
  inherit (gitignore) gitignoreSource gitignoreFilterWith;

  haskellPackageOverrides = pkgs: self: super:
    with pkgs.haskell.lib; let
      inherit (pkgs) lib;

      mkPkg = name: path: args:
        overrideCabal (self.callCabal2nix name path args) (orig: {
          src = lib.cleanSourceWith {
            name = "source";
            filter = gitignoreFilterWith {basePath = path;};
            src = path;
          };
        });
    in rec {
      split-io = mkPkg "split-io" ./. {};
    };

  mkPkgs = compiler:
    import sources.nixpkgs {
      config = {
        packageOverrides = pkgs: rec {
          haskell =
            pkgs.haskell
            // {
              packages =
                pkgs.haskell.packages
                // {
                  project = pkgs.haskell.packages."${compiler}".override {
                    overrides = haskellPackageOverrides pkgs;
                  };
                };
            };
        };
      };
    };

  mkEnvironment = compiler: let
    pkgs = mkPkgs compiler;

    haskellPackages = pkgs.haskell.packages.project;
    haskellTools = pkgs.haskell.packages."${compiler}";
  in {
    split-io = haskellPackages.split-io;

    # Make available for shell.nix
    inherit pkgs haskellPackages haskellTools;
  };
in {
  ghc902 = mkEnvironment "ghc902";
  ghc925 = mkEnvironment "ghc925";
  ghc944 = mkEnvironment "ghc944";
}
