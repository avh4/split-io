#!/bin/bash
set -euxo pipefail

dev/build.sh
nix-build -A ghc902.split-io
nix-build -A ghc925.split-io
nix-build -A ghc944.split-io
