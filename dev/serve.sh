#!/bin/bash
set -euxo pipefail

dev/build.sh
simple-http-server --index dist-newstyle/build/x86_64-linux/ghc-9.2.5/split-io-0.0.0.0/doc/html/split-io
