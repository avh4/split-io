#!/bin/bash
set -euxo pipefail

hpack
ghcid --target=split-io-tests --run
