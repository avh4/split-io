#!/bin/bash
set -euxo pipefail

hpack
cabal build
cabal test
cabal haddock
